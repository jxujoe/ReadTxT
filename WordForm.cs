﻿using System;
using System.Drawing;
using System.Windows.Forms;

namespace ReadTxT
{
    public partial class WordForm : Form
    {
        public WordForm()
        {
            InitializeComponent();
            this.Opacity = 0.7;
            this.TopMost = true;
        }

        private void WordForm_Load(object sender, EventArgs e)
        {

        }


        /******************************************/

        private Point mouseOff;//鼠标移动位置变量
        private bool leftFlag;//标签是否为左键

        private void WordForm_MouseMove(object sender, MouseEventArgs e)
        {


            if (leftFlag)
            {
                Point mouseSet = Control.MousePosition;
                mouseSet.Offset(mouseOff.X, mouseOff.Y);  //设置移动后的位置
                Location = mouseSet;
            }
        }

        private void WordForm_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                mouseOff = new Point(-e.X, -e.Y); //得到变量的值
                leftFlag = true;                  //点击左键按下时标注为true;
            }
        }

        private void WordForm_MouseUp(object sender, MouseEventArgs e)
        {
            if (leftFlag)
            {
                leftFlag = false;//释放鼠标后标注为false;
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox1.Checked == true)
            {
                this.TopMost = true;

            }
            else
            {
                this.TopMost = false;
            }

        }

        void setOpacity(int m)
        {
            double o = m / 100F;
            this.Opacity = o;

        }

        private void numericUpDown1_ValueChanged(object sender, EventArgs e)
        {
            setOpacity(int.Parse(numericUpDown1.Value.ToString()));
        }
        

        
    }
}
